# OskurBot

Un bot discord codé proprement en Python

## Dependances


* "https://github.com/Pycord-Development/pycord"


## Utilisation

Pour l'utiliser chez vous, générez une clef d'API discord, puis mettez là dans le fichier de config.
Le bot s'appelle directement via la commande ```risibot```.

* Obtenir des infos sur les arguments et options: ```risibot --help```
* Exemple d'utilisation : ```risibot -l DEBUG --log_file bot.log my_config.toml```

## Commandes actuelles

### !pouet

Affiche simplement pouet et mentionne l'utilisateur tout en rajoutant des réactions à son message.

### !tigre

Répond tigre bois

## !risi mots

Effectue une recherche sur la risibank
