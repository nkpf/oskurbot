from setuptools import find_namespace_packages, setup

setup(
    name="Oskurbot",
    version="1.0.0",
    packages=find_namespace_packages(),
    py_modules=["app"],
    install_requires=[
        "Click",
        "pytomlpp",
        "requests",
        "py-cord",
    ],
    entry_points={
        "console_scripts": [
            "risibot = app:oskurbot",
        ],
    },
)
