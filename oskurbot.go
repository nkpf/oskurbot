package main
import (
	"os"
	"os/signal"
	"strings"
	"syscall"
	"io/ioutil"
	"log"
	
	"oskurbot/commands"
	
	"github.com/bwmarrin/discordgo"
	"github.com/pelletier/go-toml"
	"github.com/go-redis/redis/v8"  
)
var token string

func init(){
	var redis_host string
	token, redis_host = readConfig()

	rdb := redis.NewClient(&redis.Options{
		Addr: redis_host,
		Password: "",
		DB: 0,
	})
	commands.InitPackage(rdb)
}

func main(){
	dg, err := discordgo.New("Bot " + token)
	if err != nil {
		log.Println("Error creating Discord session: ", err)
		return
	}
	// Register ready as a callback for the ready events.
	dg.AddHandler(ready)

	// Register messageCreate as a callback for the messageCreate events.
	dg.AddHandler(messageCreate)

	// Register guildCreate as a callback for the guildCreate events.
	dg.AddHandler(guildCreate)

	dg.AddHandler(messageReactionAdd)
	dg.AddHandler(messageReactionRemove)

	//dg.AddHandler(typingStart)
	// Open the websocket and begin listening.
	err = dg.Open()
	if err != nil {
		log.Println("Error opening Discord session: ", err)
	}
	// Wait here until CTRL-C or other term signal is received.
	log.Println("OskurBot is now running.  Press CTRL-C to exit.")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc

	// Cleanly close down the Discord session.
	dg.Close()
}


func readConfig() (key string, redis_host string) {
	fichier, err := ioutil.ReadFile("./config.toml")
	if err != nil {
		log.Fatal(err)
	}
	config, _ := toml.Load(string(fichier))
	key = config.Get("discord.token").(string)
	redis_host = config.Get("redis.host_name").(string)
	return key, redis_host
}

// This function will be called (due to AddHandler above) when the bot receives
// the "ready" event from Discord.
func ready(s *discordgo.Session, event *discordgo.Ready) {

	// Set the playing status.
	s.UpdateGameStatus(0, "Ayaaaaaaaaaaa")
}


// This function will be called (due to AddHandler above) every time a new
// message is created on any channel that the autenticated bot has access to.
func messageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {

	// Ignore all messages created by the bot itself
	// This isn't required in this specific example but it's a good practice.
	if m.Author.ID == s.State.User.ID {
		return
	}

	if strings.HasPrefix(m.Content, "?aled") {
		commands.Help(s,m)
	}
	// Commande !pouet
	if strings.HasPrefix(m.Content, "?pouet") {
		commands.Pouet(s,m)
	}
	// Commande !tigrebois
	if strings.HasPrefix(m.Content, "?tigre") {
		commands.TigreBois(s,m)
	}

	if strings.HasPrefix(m.Content, "?cat") {
		commands.Cat(s,m)
	}
	// Commande !risi
	if strings.HasPrefix(m.Content, "?risi") {
		commands.Risitas(s,m,false)
	}

	if strings.HasPrefix(m.Content, "?rand") {
		commands.Risitas(s,m,true)
	}

	commands.CountEmojis(s,m)
	
	if strings.HasPrefix(m.Content, "?top") {
		commands.Ranking(s,m)
	}
	
	if strings.HasPrefix(m.Content, "?rank") {
		commands.Rank(s,m)
	}

	if strings.HasPrefix(m.Content, "?emojiRank") {
		commands.EmojiRank(s,m)
	}

	if strings.HasPrefix(m.Content, "?classement") {
		commands.EmojiRanking(s,m)
	}

	if strings.HasPrefix(m.Content, "?migrate") && m.Author.ID == "188315888397582337" {
		commands.ComputeEmojiSum(s,m)
	}
}

// This function will be called (due to AddHandler above) every time a new
// guild is joined.
func guildCreate(s *discordgo.Session, event *discordgo.GuildCreate) {

	if event.Guild.Unavailable {
		return
	}

	for _, channel := range event.Guild.Channels {
		if channel.ID == event.Guild.ID {
			
			return
		}
	}
}

func messageReactionAdd(s *discordgo.Session, reaction *discordgo.MessageReactionAdd) {
	commands.ZIncreaseWrapper(reaction.Emoji.ID, 1, reaction.UserID)
	commands.ZIncreaseWrapper(reaction.GuildID, 1, reaction.Emoji.ID)
}

func messageReactionRemove(s *discordgo.Session, reaction *discordgo.MessageReactionRemove) {
	commands.ZIncreaseWrapper(reaction.Emoji.ID, -1, reaction.UserID)
	commands.ZIncreaseWrapper(reaction.GuildID, -1, reaction.Emoji.ID)
}
