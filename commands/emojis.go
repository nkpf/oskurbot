package commands
import (
	"strings"
	"log"
	"fmt"
	"regexp"
	"errors"
	"time"
	
	"github.com/bwmarrin/discordgo"
	"github.com/Necroforger/dgwidgets"

)

func getNameOrNick(user *discordgo.Member) (name string) {
	if user.Nick != "" {
		return user.Nick
	} else {
		return user.User.Username
	}
}

func CountEmojis(s *discordgo.Session, m *discordgo.MessageCreate) {
	emojis, err := s.GuildEmojis(m.GuildID)
	if err != nil {
		log.Println(err)
		return
	}
	for _, emoji := range emojis {
		count := float64(strings.Count(m.Content, emoji.MessageFormat()))
		if count > 0 {
			ZIncreaseWrapper(emoji.ID, count, m.Author.ID)
			
			ZIncreaseWrapper(m.GuildID, count, emoji.ID)
		}
	}
}

func extractEmojiID(s *discordgo.Session, m *discordgo.MessageCreate, c *discordgo.Channel) (emojiID string, formetedEmoji string, err error) {
	sequence := strings.Split(m.Content, " ")
	if len(sequence) == 2 {
		re := regexp.MustCompile(`[0-9]+`)
		emojiID = re.FindString(sequence[1])
		if emojiID == "" {
			_,_ = s.ChannelMessageSend(c.ID,"Aucun emoji trouvé")
			return "", "",nil
		}
		return emojiID, sequence[1], nil
	}
	return "", "", errors.New("Message have less or more than 2 words")
}

func rankCommand(s *discordgo.Session, m *discordgo.MessageCreate, c *discordgo.Channel, set string, member string, title string, quantityTitle string, imageURL string) {

	rank, err := rdb.ZRevRank(ctx, set, member).Result()
	if err != nil {
		log.Println(err)
	}
	total, err := rdb.ZCount(ctx, set, "-inf", "+inf").Result()
	count, err := rdb.ZScore(ctx, set, member).Result()
	
	rank = rank+1

	pos := fmt.Sprintf("%d/%d", rank, total)
	qty := fmt.Sprintf("%d", int(count))
	var message = discordgo.MessageEmbed{
		Title: title,
		Thumbnail: &discordgo.MessageEmbedThumbnail{
			URL: imageURL,
		},
		Fields: []*discordgo.MessageEmbedField{
			{
				Name: "Position",
				Value: pos,
			},
			{
				Name: quantityTitle,
				Value: qty,
			},
		},
	}
	_,err = s.ChannelMessageSendEmbed(c.ID, &message)

}

func Rank(s *discordgo.Session, m *discordgo.MessageCreate) {
	c, err := s.State.Channel(m.ChannelID)
	if err != nil {
		// Could not find channel.
		return
	}
	user, err := s.GuildMember(m.GuildID, m.Author.ID)
	if err != nil {
		log.Println(err)
	}	
	name := getNameOrNick(user)
	emojiID, emojiFormated, err := extractEmojiID(s, m, c)
	if err != nil {
		_,_ = s.ChannelMessageSend(c.ID,"Utilisation ?rank [emoji]")
		return
	}
	title := "Classement de "+name
	quantityTitle := "Quantité de " + emojiFormated
	rankCommand(s, m, c, emojiID, m.Author.ID, title, quantityTitle, m.Author.AvatarURL(""))

}

func EmojiRank(s *discordgo.Session, m *discordgo.MessageCreate) {
	c, err := s.State.Channel(m.ChannelID)
	if err != nil {
		// Could not find channel.
		return
	}
	
	emojiID, emojiFormated, err := extractEmojiID(s, m, c)
	if err != nil {
		_,_ = s.ChannelMessageSend(c.ID,"Utilisation ?total [emoji]")
		return
	}
	title := "Classement de "+emojiFormated
	quantityTitle := "Total de "+emojiFormated
	rankCommand(s, m, c, m.GuildID, emojiID, title, quantityTitle,"")
	
}

func Ranking(s *discordgo.Session, m *discordgo.MessageCreate) {
	
	p := dgwidgets.NewPaginator(s, m.ChannelID)


	sequence := strings.Split(m.Content, " ")
	if len(sequence) == 2 {
		re := regexp.MustCompile(`[0-9]+`)
		emojiID := re.FindString(sequence[1])
		if emojiID == "" {
			_,_ = s.ChannelMessageSend(m.ChannelID,"Aucun emoji trouvé")
			return 
		}
		ranking, err := rdb.ZRevRangeWithScores(ctx, emojiID, 0, -1).Result()
		if err != nil {
			log.Println(err)
		}

		var fields []*discordgo.MessageEmbedField
		members, _  := s.GuildMembers(m.GuildID, "", 1000)
		
		for i, rank := range ranking {
			for _, member := range members {
				if (rank.Member.(string) == member.User.ID) {
					score := int(rank.Score)
					if err != nil {
						log.Println(err)
					}
					name := getNameOrNick(member)
					field := fmt.Sprintf("%s : %d", name, score)
					pos := fmt.Sprintf("%d", i+1)
					fields = append(fields, &discordgo.MessageEmbedField{
						Name: pos,
						Value: field,
					})
				}
			}
			if len(fields) == 10 {
				tmp := make([]*discordgo.MessageEmbedField, len(fields))
				copy(tmp, fields)
				p.Add(&discordgo.MessageEmbed{
					Title: "Classement - "+sequence[1],
					Fields: tmp,
				})
				fields = fields[:0]
			}
		}
		if len(fields) > 0 {
			p.Add(&discordgo.MessageEmbed{
				Title: "Classement - "+sequence[1],
				Fields: fields,
			})
		}

		// Sets the footers of all added pages to their page numbers.
		p.SetPageFooters()

		// When the paginator is done listening set the colour to yellow
		p.ColourWhenDone = 0xffff

		// Stop listening for reaction events after five minutes
		p.Widget.Timeout = time.Minute * 5

		p.Spawn()
	} else {
		_,_ = s.ChannelMessageSend(m.ChannelID,"Utilisation ?ranking [emoji]")
	}

}

func EmojiRanking(s *discordgo.Session, m *discordgo.MessageCreate) {
	
	ranking, err := rdb.ZRevRangeWithScores(ctx, m.GuildID, 0, -1).Result()
	if err != nil {
		log.Println(err)
	}

	var fields []*discordgo.MessageEmbedField
	emojis, err := s.GuildEmojis(m.GuildID)

	p := dgwidgets.NewPaginator(s, m.ChannelID)


	for i, rank := range ranking {
		for _, emoji := range emojis {
			if (rank.Member.(string) == emoji.ID) {
				score := int(rank.Score)
				if err != nil {
					log.Println(err)
				}
				field := fmt.Sprintf("%s : %d", emoji.MessageFormat(), score)
				pos := fmt.Sprintf("%d", i+1)
				fields = append(fields, &discordgo.MessageEmbedField{
					Name: pos,
					Value: field,
					Inline: true,
				})
			}
		}
		if len(fields) == 20 {
			tmp := make([]*discordgo.MessageEmbedField, len(fields))
			copy(tmp, fields)
			p.Add(&discordgo.MessageEmbed{
				Title: "Classement des emojis",
				Fields: tmp,
			})
			fields = fields[:0]
		}
	}
	if len(fields) > 0 {
		p.Add(&discordgo.MessageEmbed{
			Title: "Zboui des emojis",
			Fields: fields,
		})
	}


	// Sets the footers of all added pages to their page numbers.
	p.SetPageFooters()

	// When the paginator is done listening set the colour to yellow
	p.ColourWhenDone = 0xffff

	// Stop listening for reaction events after five minutes
	p.Widget.Timeout = time.Minute * 5

	p.Spawn()
}

