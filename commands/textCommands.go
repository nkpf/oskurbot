package commands
import (
	"github.com/bwmarrin/discordgo";
)

func Pouet(s *discordgo.Session,m *discordgo.MessageCreate) {
	c, err := s.State.Channel(m.ChannelID)
	if err != nil {
		return
	}
	
	_,_ = s.ChannelMessageSend(c.ID,"Pouet "+m.Author.Mention()+"!");
}

func TigreBois(s *discordgo.Session,m *discordgo.MessageCreate) {
	c, err := s.State.Channel(m.ChannelID)
	if err != nil {
		return
	}
	
	_,_ = s.ChannelMessageSend(c.ID,"Tigre Bois 🐅");
}

func Help(s *discordgo.Session, m *discordgo.MessageCreate) {
	c, err := s.State.Channel(m.ChannelID)
	if err != nil {
		return
	}
	
	var message = discordgo.MessageEmbed{
		Title: "Liste des commandes",
		Fields: []*discordgo.MessageEmbedField{
			{
				Name: "?aled",
				Value: "Affiche cette aide",
			},
			{
				Name: "?pouet",
				Value: "pouet",
			},
			{
				Name: "?tigrebois",
				Value: "Tigreeeeeeuh bwwaaaaaa",
			},
			{
				Name: "?cat [code HTTP]",
				Value: "Utilisation ?cat [code HTTP]",
			},
			{
				Name: "?risi [termes à chercher]",
				Value: "Va chercher sur la risibank le premier risitas correspondant aux différents termes de recherches (accents interdits)",
			},
			{
				Name: "?rand [termes à chercher]",
				Value: "Comme ?risi mais retourne un risitas aléatoire",
			},
			{
				Name: "?rank [emoji]",
				Value: "Votre rang pour l'émoji choisi",
			},
			{
				Name: "?top [emoji]",
				Value: "Le top10 pour l'émoji choisi",
			},
			{
				Name: "?emojiRank [emoji]",
				Value: "Le rang de l'emoji par rapport aux autres",
			},			
			{
				Name: "?classement",
				Value: "Le classement des emojis!",
			},
			
		},
	}
	_,_ = s.ChannelMessageSendEmbed(c.ID, &message)
}
