package commands

import (
	"log"
	"context"
	
	"github.com/go-redis/redis/v8"
	"github.com/bwmarrin/discordgo"
)

var rdb *redis.Client
var ctx context.Context

func InitPackage(redisClient *redis.Client) {
	rdb = redisClient
	ctx = context.Background()
	pong, err := rdb.Ping(ctx).Result()
	log.Println(pong, err)
}

func ZIncreaseWrapper(set string, score float64, member string) {
	_, err := rdb.ZIncr(ctx, set, &redis.Z{
		Score: score,
		Member: member,
	}).Result()

	if err != nil {
		log.Println(err)
		return
	}
}

func ComputeEmojiSum(s *discordgo.Session, m *discordgo.MessageCreate) {

	log.Println("Starting aggregate computation...")

	emojis, err := s.GuildEmojis(m.GuildID)
	if err != nil {
		log.Println(err)
		return
	}
	
	for _, emoji := range emojis {
		ranking, err := rdb.ZRevRangeWithScores(ctx, emoji.ID, 0, -1).Result()
		if err != nil {
			log.Println(err)
			return
		}
		sum := 0.0
		for _, rank := range ranking {
			sum = sum + rank.Score
		}
		_, err = rdb.ZAdd(ctx, m.GuildID, &redis.Z{
			Score: sum,
			Member:  emoji.ID,
		}).Result()
		if err != nil {
			log.Println(err)
			return
		}
	}

	log.Println("Computation complete!")
}
