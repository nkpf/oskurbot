import inspect
import logging

from discord.ext.commands.core import Command


def logged_function(logger: logging.Logger):
    """The function that logs the given logging function.

    Args:
        logger (logging.Logger): The logger used to log the execution of the function
    """

    def handle_func(func):
        async def wrap(*args, **kwargs):
            logger.info(f"The function {func.__name__} is called")
            res = await func(*args, **kwargs)
            logger.debug(f"The function {func.__name__} has been executed")
            return res

        return wrap

    return handle_func


def logged_command(
    logger: logging.Logger = None, name: str = None, cls: type = None, **attrs
):
    """Decorator to wrap a callback to a Command .

    Args:
        logger (logging.Logger, optional): The logger used to log the command, replaced by
            logging if set to None. Defaults to None.
        name (str, optional): The command's name. Defaults to None.
        cls (type, optional): The class to handle command. Defaults to None.

    Raises:
        TypeError: Raises if the input of the decorator is already a Command object

    Returns:
        function: The decorated function
    """
    if cls is None:
        cls = Command

    if logger is None:
        logger = logging

    def decorator(func):
        if isinstance(func, Command):
            raise TypeError("Callback is already a command.")
        selected_name = func.__name__ if name is None else name
        selected_help = (
            inspect.getdoc(func) if "help" not in attrs.keys() else attrs["help"]
        )
        return cls(
            logged_function(logger)(func),
            name=selected_name,
            help=selected_help,
            **attrs,
        )

    return decorator
