import logging

from discord.ext.commands.cog import Cog
from discord import Member
from discord.ext.commands.context import Context
from oskurbot.features.risibank.risibank_extractor import RisibankExtractor

from oskurbot.logging import logged_command

logger = logging.getLogger(__name__)

ERROR_MESSAGE = "Mauvaise recherche"


class RisibankCommandsCog(Cog):
    """Cog class with commands to interact with the risibank.fr website"""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.extractor = RisibankExtractor()

    def _search(self, text_search):
        try:
            return self.extractor.search(text_search)
        except Exception as e:
            logger.error(e)
            return []

    def _parse_message(self, message):
        return message[6:]  # "?risi " or "?rand " are 6 characters long

    @logged_command(logger=logger)
    async def risi(self, ctx: Context, member: Member = None):
        """Retourne le premier résultat de risibank."""
        result_imgs = self._search(self._parse_message(ctx.message.content))
        res_message = ERROR_MESSAGE
        if result_imgs:
            res_message = result_imgs.select_first()
        await ctx.send(res_message)

    @logged_command(logger=logger)
    async def rand(self, ctx: Context, member: Member = None):
        """Retourne aléatoirement un résultat de risibank"""
        result_imgs = self._search(self._parse_message(ctx.message.content))
        res_message = ERROR_MESSAGE
        if result_imgs:
            res_message = result_imgs.select_randomly()
        await ctx.send(res_message)
