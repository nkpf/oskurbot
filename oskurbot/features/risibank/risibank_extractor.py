import re
import json
import random
import requests


class BadRisibankInput(Exception):
    pass


class RisibankResult:
    def __init__(self, response):
        dict_response = json.loads(response.content)
        if "data" not in dict_response:
            raise BadRisibankInput("The JSON input doesn't have a data input")
        self.links = [res["risibank_link"] for res in dict_response["data"]]

    def select_randomly(self):
        return random.choice(self.links)

    def select_first(self):
        return self.links[0]


class RisibankExtractor:
    def __init__(self):
        self.session = requests.Session()
        self.token = re.findall(
            r"laravel_csrf\s=\s\".*\";",
            self.session.get("https://risibank.fr").content.decode("utf-8"),
        )[0].split('"')[1]

    def search(self, search_text):
        return RisibankResult(
            self.session.post(
                "https://risibank.fr/stickers/rechercher",
                data={"search": {search_text}, "offset": "0", "_token": {self.token}},
            )
        )
