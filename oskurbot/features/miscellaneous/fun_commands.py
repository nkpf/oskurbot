import logging

from discord.ext.commands.cog import Cog
from discord import Member
from discord.ext.commands.context import Context

from oskurbot.logging import logged_command

logger = logging.getLogger(__name__)

HTTP_ERROR_CODES = (
    "100",
    "101",
    "102",
    "103",
    "200",
    "201",
    "202",
    "203",
    "204",
    "205",
    "206",
    "207",
    "208",
    "210",
    "226",
    "300",
    "301",
    "302",
    "303",
    "304",
    "305",
    "306",
    "307",
    "308",
    "310",
    "401",
    "402",
    "403",
    "404",
    "405",
    "406",
    "407",
    "408",
    "409",
    "410",
    "411",
    "412",
    "413",
    "414",
    "415",
    "416",
    "417",
    "418",
    "421",
    "422",
    "423",
    "424",
    "425",
    "426",
    "428",
    "429",
    "431",
    "449",
    "450",
    "451",
    "456",
    "444",
    "495",
    "496",
    "497",
    "498",
    "499",
    "500",
    "501",
    "502",
    "503",
    "504",
    "505",
    "506",
    "507",
    "508",
    "509",
    "510",
    "511",
    "520",
    "521",
    "522",
    "523",
    "524",
    "525",
    "526",
    "527",
)


class FunCommandsCog(Cog):
    """Cog class that adds some simple commands to the bot."""

    @logged_command(logger=logger)
    async def pouet(self, ctx: Context, member: Member = None):
        """Pouet pouet pouet"""
        member = member or ctx.author
        await ctx.send(f"Pouet {member.name}")

    @logged_command(logger=logger, name="tigrebois")
    async def tigre_bois(self, ctx: Context, member: Member = None):
        """Tigreeeeeeuh bwwaaaaaa"""
        await ctx.send("Tigre Bois 🐅")

    @logged_command(logger=logger)
    async def cat(self, ctx: Context, member: Member = None):
        """Affiche des images de chats correspondant à des codes HTTP"""
        http_code = ctx.message.content[5:]
        if http_code in HTTP_ERROR_CODES:
            await ctx.send(f"https://http.cat/{http_code}")
        else:
            await ctx.send("Utilisation: `?cat [code HTTP]`")

    @logged_command(logger=logger)
    async def gneuh(self, ctx: Context, member: Member = None):
        """Met en capital une lettre sur deux."""
        text = ctx.message.content[7:]
        text_capitalized = ""
        for i, selected_letter in enumerate(text):
            if i % 2 == 0:
                text_capitalized += selected_letter.upper()
            else:
                text_capitalized += selected_letter.lower()
        await ctx.send(f"{text_capitalized}")
