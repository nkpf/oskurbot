import logging
from discord.ext.commands import Bot, Context
from discord.ext.commands.cog import Cog
from discord.message import Message

logger = logging.getLogger(__name__)

DEFAULT_HELP_COMMAND = "help"


class OskurBot(Bot):
    """
    The Discord bot of the application. It handles a TOML config file (not supported yet),
    you can give it all the cogs you want to use with your bot.
    """

    def __init__(
        self,
        *cogs,
        description="",
        command_prefix="?",
        case_insensitive=False,
        help_command_name="help",
        only_one_help_command=False,
        **kwargs,
    ):
        super().__init__(
            description=description,
            command_prefix=command_prefix,
            case_insensitive=case_insensitive,
            **kwargs,
        )
        for cog in cogs:
            self.add_cog(cog)

        if help_command_name not in self.all_commands:
            cmd_str = help_command_name
            cmd_str += "/help" if not only_one_help_command else ""
            self._help_command._command_impl.name = cmd_str
            self.all_commands[help_command_name] = self.all_commands[
                DEFAULT_HELP_COMMAND
            ]
            if only_one_help_command:
                del self.all_commands[DEFAULT_HELP_COMMAND]

    async def get_context(self, message: Message, cxt: type = Context) -> Context:
        """Get a context for the given message.

        Args:
            message (Message): The message to handle
            cxt (type, optional): The context class to use. Defaults to Context.

        Returns:
            Context: the Context Object
        """
        # Default context, can be overwritten
        return await super().get_context(message, cls=cxt)

    async def on_command_error(self, context: Context, exception: Exception):
        """Handle a command error.

        Args:
            context (Context): The context of the message
            exception (Exception): The exception catched
        """
        if self.extra_events.get("on_command_error", None):
            return

        if hasattr(context.command, "on_error"):
            return

        cog = context.cog
        if cog and Cog._get_overridden_method(cog.cog_command_error) is not None:
            return

        command = context.command
        user = context.author
        logger.error(
            f"Ignoring exception in command {command} launched by {user}: {exception}"
        )
        await context.send(
            "The command doesn't exist.\nUse `?help` command to see the complete listing."
        )
