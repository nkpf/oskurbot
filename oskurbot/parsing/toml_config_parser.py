import pytomlpp

from .bot_config import BotConfiguration


class TomlConfigParser:
    def __init__(self):
        self.discord = None
        self.redis = None
        self.bot = None

    def parse_config(self, filepath):
        data = pytomlpp.load(filepath)
        self.discord = data["discord"]
        self.redis = data["redis"]
        self.bot = BotConfiguration(**data["bot"])
