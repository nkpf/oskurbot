from dataclasses import dataclass, asdict


@dataclass
class BotConfiguration:
    """A dataclass with all the options available for the bot."""

    description: str
    command_prefix: str
    case_insensitive: bool
    help_command_name: str
    only_one_help_command: bool

    def to_dict(self) -> dict:
        """Return a dict representation of the object.

        Returns:
            dict: The corresponding dict representation
        """
        return asdict(self)
