from oskurbot.oskurbot import OskurBot
from oskurbot.features.miscellaneous import FunCommandsCog
from oskurbot.features.risibank import RisibankCommandsCog
from oskurbot.parsing.toml_config_parser import TomlConfigParser
