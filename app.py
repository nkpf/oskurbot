import logging

# TODO: using argparse to loads config file + logger options
import click
import pathlib

from oskurbot import OskurBot, TomlConfigParser, FunCommandsCog, RisibankCommandsCog


@click.command()
@click.argument(
    "config_file", type=click.Path(exists=True, dir_okay=False, path_type=pathlib.Path)
)
@click.option(
    "-l",
    "--log_level",
    type=click.Choice(["DEBUG", "INFO", "WARNING"], case_sensitive=False),
    default="WARNING",
    help="Changes the log level between debug, info and warning",
)
@click.option(
    "--log_file",
    required=True,
    type=click.Path(),
    help="Specify the path of the log file",
    default="bot.log"
)
def oskurbot(config_file, log_level, log_file):
    """
    Launches the bot with a given CONFIG FILE.
    """
    numeric_level = getattr(logging, log_level.upper(), None)
    logging.basicConfig(
        filename=log_file,
        level=numeric_level,
        encoding="utf-8",
        format="%(asctime)s:%(levelname)s:%(name)s: %(message)s"
    )
    logger = logging.getLogger("__name__")

    config = TomlConfigParser()
    config.parse_config(config_file)

    logger.info(f"Bot configuration: {config.bot}")
    bot = OskurBot(FunCommandsCog(), RisibankCommandsCog(), **config.bot.to_dict())
    bot.run(config.discord["token"])
