FROM python:3.9-bullseye

WORKDIR oskurbot

ADD . .

# Not using pipenv because pipenv sucks for docker images
RUN pip install .

CMD risibot -l info --log_file bot.log config.toml
